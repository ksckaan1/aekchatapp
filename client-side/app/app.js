const $ = require('jquery'); //jQuery Kütüphanesini ekle
const { remote }= require('electron'); //Electron yönetim modülünü ekle


var win =remote.getCurrentWindow(); //win değişkenini pencere kontrolü olarak ayarla
function kapat(){
    $('#ekran').css("filter","blur(8px)");   
    setTimeout(function(){win.close();},700);
}
function kucult(){
    win.minimize();
}
function link(){
    var sonuc = document.getElementById('ekran').contentWindow.location.href;
    $('#appname').html(sonuc);
}
var yanmenu = 0;
function menuacik() {    
    $('#yanmenu').animate({left: '0px'}, "fast");
    $('#ekran').animate({left: '200px'}, "fast");
    $('#ekran').css("filter","blur(8px)").css("border-radius","0px 0px 0px 0px");
    yanmenu=1;
}
function menukapali() {
    $('#yanmenu').animate({left: '-220px'}, "fast");
        $('#ekran').animate({left: '0px'}, "fast");
        $('#ekran').css("filter","blur(0px)").css("border-radius","0px 0px 10px 10px");
        yanmenu=0;
}
function yanmenubuton(){
    if(yanmenu==1){menukapali();}
    else{menuacik();}
    
}

function sohbetgoster(){
    $('#ekran').attr('src','http://kaanksc.com/aek/anasayfam.php');
    menukapali();
}
function sitegoster(){
    $('#ekran').attr('src','http://kaanksc.com');
    menukapali();
}
function istemciyenile(){
    win.reload();
    menukapali();
    $('#ekran').css("filter","blur(8px)");    
    win.webContents.session.clearCache(function(){});
}
function cikisyap(){
    $('#ekran').attr('src','http://kaanksc.com/aek/cikis.php');
    menukapali();
}
function logged(){
    var sonuc = document.getElementById('ekran').contentWindow.location.href;
    if(sonuc=='http://kaanksc.com/aek/anasayfam.php' || sonuc=='http://kaanksc.com/aek/admin.php' || sonuc=='http://kaanksc.com/aek/duzenle.php'){
        $('#online').show();
        $('#offline').hide();
    }else{
        $('#online').hide();
        $('#offline').show();
    }
}
setInterval(logged, 300);
