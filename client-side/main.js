const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');

let win;

function createWindow(){
    win = new BrowserWindow({
        width: 485,
        height: 483,
        resizable: false,
        frame: false,
        transparent: true,
        backgroundColor: '#00FFFFFF'
    });
    win.loadURL('file://' + __dirname + '/app/app.html');
    win.setMaximizable(false);
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if(process.platform !== 'darwin'){
        app.quit();
    }
});